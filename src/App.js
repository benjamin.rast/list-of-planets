import React, {Component} from 'react';
import api from './utils/api';
import Dropdown from './components/Dropdown';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      planets: [],
      totalPlanets: 0,
      params: {
        page: 1,
        search: '',
      }
    }
  }

  componentDidMount() {
    console.log('component DID Mount')
    api.get('/planets.json').then(response => {
      const { data: {results, count} } = response;

      this.setState({
        planets: results,
        totalPlanets: count
      })
    });
  }


  render() {
    const optionsToDisplay = this.state.planets.map(v => v.name);

    return (
      <div className="App">
        <h1>Total: {this.state.totalPlanets} </h1>
        <div className="App__container">
          <Dropdown
            options={optionsToDisplay}
          />
        </div>
        <ul>
          {this.state.planets.map((v, index) => {
            return <li key={index}>{v.name} - {v.climate}</li>
          })}
        </ul>
      </div>
    );
  }
}

export default App;
