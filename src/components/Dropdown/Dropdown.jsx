import React, {Component} from 'react';
import './_dropdown.scss';

class Dropdown extends Component {
  state = {
    isOpen: false,
    selected: ''
  };

  handleClick = () => {
    const {isOpen} = this.state;
    this.setState({isOpen: !isOpen})
  };

  handleOptionClick = (name) => () => {
    this.setState({selected: name});
  };

  render() {
    const {isOpen, selected} = this.state;
    const {options} = this.props;

    const optionsParsed = options || [];

    return (
      <div className="dropdown">
        <div className="dropdown__header" onClick={this.handleClick}>
          { selected ? selected : 'Header'}
        </div>
        <div className={`dropdown__options ${isOpen ? 'open' : ''}`}>
          <ul>
            {optionsParsed.map((v, i) => {
              const isSelected = selected === v;
              return (
                <li className={`${isSelected ? 'selected' : ''}`} key={i} onClick={this.handleOptionClick(v)}>
                  {v}
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default Dropdown;
